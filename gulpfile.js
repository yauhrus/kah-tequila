const plumber = require('gulp-plumber');
const fileinclude = require('gulp-file-include');
const rename = require('gulp-rename');
const gulpCopy = require('gulp-copy');
const postcss = require('gulp-postcss');
const uglify = require('gulp-uglify');
const terser = require('gulp-terser');
const concat = require('gulp-concat');
const postCssImport = require('postcss-import');
const autoprefixer = require('autoprefixer');
const browsersync = require('browser-sync').create();
const cssnano = require('cssnano');
const gulp = require('gulp');
const del = require('del');
const cssvariables = require('postcss-css-variables');
const nested = require('postcss-nested');
const nesting = require('postcss-nesting');
const postCssCustomMedia = require('postcss-custom-media');
const postCssCustomProperties = require('postcss-custom-properties');

// BrowserSync
function browserSync(done) {
  browsersync.init({
    server: {
      baseDir: './dist/'
    },
    port: 3000,
    open: false,
  });
  done();
}

// BrowserSync Reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}

// Clean assets
function clean() {
  return del(['./dist/']);
}

// CSS task
function css() {
  return gulp
    .src(['./src/css/main.css'],{ sourcemaps: true })
    .pipe(plumber())
    .pipe(postcss([
      postCssImport,
      // postCssCustomProperties(),
      postCssCustomMedia(),
      cssvariables(),
      nested(),
      nesting(),
      autoprefixer,
      cssnano({ discardComments: { removeAll: true } }),
    ]))
    .pipe(concat('main.css'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./dist/css/', {sourcemaps: '.'}))
    .pipe(browsersync.stream());
}

// Transpile, concatenate and minify scripts
function scripts() {
  return (
    gulp
      .src(['./src/js/main.js'])
      .pipe(plumber())
      .pipe(concat('main.js'))
      .pipe(terser({
        mangle: false,
      }))
      .pipe(rename({ suffix: '.min' }))
      .pipe(gulp.dest('./dist/js/'))
      .pipe(browsersync.stream())
  );
}

function buildVendorsScripts() {
  return (
  gulp
    .src(['./src/js/swiper-bundle.min.js'])
    .pipe(plumber())
    .pipe(concat('vendors.js'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./dist/js/'))
    .pipe(browsersync.stream())
  );
}

// Transpile, concatenate html
function html() {
  return (
    gulp
      .src(['./src/views/**/*.html'])
      .pipe(fileinclude({
        prefix: '@@',
        basepath: '@file'
      }))
      .pipe(gulp.dest('./dist'))
      .pipe(browsersync.stream())
  );
}

function copyFiles() {
  return (
    gulp
      .src(['src/uploads/*'])
      .pipe(gulp.dest('./dist/uploads/'))
  );
}
function copyFonts() {
  return (
    gulp
      .src(['src/fonts/**/*'])
      .pipe(gulp.dest('./dist/fonts/'))
  );
}

function copySvg() {
  return (
    gulp
      .src(['src/svg/*'])
      .pipe(gulp.dest('./dist/svg/'))
  );
}

// function copyVideo() {
//   return (
//     gulp
//       .src(['src/video/*'])
//       .pipe(gulp.dest('./dist/video/'))
//   );
// }

function copyImages() {
  return (
    gulp
      .src(['src/img/**/*'])
      .pipe(gulp.dest('./dist/img/'))
  );
}

// function copyOldHtml() {
//   return (
//     gulp
//       .src(['src/*.html'])
//       .pipe(gulp.dest('./dist/'))
//   );
// }

// Watch files
function watchFiles() {
  gulp.watch('./src/css/**/*', css);
  gulp.watch('./src/js/**/*', scripts);
  gulp.watch(
    [
      './src/views/**/*',
      './src/partials/**/*',
    ],
    gulp.series(html, browserSyncReload)
  );
}

// define complex tasks
const build = gulp.series(
  clean,
  gulp.parallel(
    css,
    html,
    scripts,
    buildVendorsScripts,
    copyFonts,
    copyFiles,
    copySvg,
    copyImages,
    // copyOldHtml,
    // copyVideo
  )
);
const watch = gulp.parallel(watchFiles, browserSync);
const dev = gulp.series(build, watch);

// export tasks
exports.css = css;
exports.scripts = scripts;
exports.html = html;
exports.dev = dev;
exports.clean = clean;
exports.build = build;
exports.watch = watch;
exports.default = build;
exports.copyFiles = copyFiles;
exports.copyImages = copyImages;