const headerBurger = document.querySelector('.headerBurger');
const menu = document.querySelector('.menu');
const menuClose = document.querySelector('.menuClose');

document.body.classList.add('popupOpened');

headerBurger.addEventListener('click', function() {
  // this.classList.toggle('active');
  menu.classList.add('active');
  document.body.classList.add('popupOpened');
});

menuClose.addEventListener('click', function() {
  menu.classList.remove('active');
  document.body.classList.remove('popupOpened');
});

/* Age verification & cookies */

const ageVerification = document.querySelector('.ageVerification');
const ageVerificationConfirm = document.querySelector('.ageVerificationButton--confirm');
const ageVerificationDecline = document.querySelector('.ageVerificationButton--decline');
const cookies = document.querySelector('.cookies');
const cookiesClose = document.querySelector('.cookiesClose');
const cookiesAccept = document.querySelector('.cookiesButton--accept');
const cookiesOptions = document.querySelector('.cookiesButton--options');

ageVerificationConfirm.addEventListener('click', function() {
  ageVerification.classList.remove('active');
  document.body.classList.remove('popupOpened');
});

ageVerificationDecline.addEventListener('click', function() {
  ageVerification.classList.remove('active');
  document.body.classList.remove('popupOpened');
});

cookiesClose.addEventListener('click', function() {
  cookies.classList.remove('active');
});

cookiesAccept.addEventListener('click', function() {
  cookies.classList.remove('active');
});

cookiesOptions.addEventListener('click', function() {
  cookies.classList.remove('active');
});

/* Products slider */

let productsSlider = new Swiper('.productsContainer', {
  centeredSlides: true,
  loop: true,
  effect: 'slide',
  speed: 300,
  grabCursor: true,
  breakpoints: {
    320: {
      slidesPerView: 2,
    },
    769: {
      slidesPerView: 3,
    },
  }
});

/* Tequila slider */ 

let tequilaSlider = new Swiper('.tequilaProductContainer', {
  slidesPerView: 1,
  centeredSlides: true,
  loop: true,
  effect: 'slide',
  speed: 400,
  grabCursor: false,
});

/* Tequila Bottle size */

const tequilaSizes = document.querySelectorAll('.tequilaProductSizeItem');

tequilaSizes.forEach(function(item) {
  item.addEventListener('click', function(e) {
    if(!e.target.classList.contains('active')) {
      document.querySelector('.tequilaProductSizeItem.active').classList.remove('active');
      this.classList.add('active');
      let tequilaSlide = document.querySelector('.swiper-slide[data-size="' + this.dataset.size + '"]');
      tequilaSlider.slideTo(parseInt(tequilaSlide.dataset.swiperSlideIndex) + 1, 400, false);
    }
  });
});

/* Posts slider */

let postsSlider = new Swiper('.moreAboutContainer', {
  centeredSlides: true,
  loop: true,
  effect: 'slide',
  speed: 400,
  grabCursor: true,
  breakpoints: {
    320: {
      slidesPerView: 1.8,
      spaceBetween: 15,
    },
    601: {
      slidesPerView: 3.5,
      spaceBetween: 20,
    },
    769: {
      slidesPerView: 5,
      spaceBetween: 30,
    },
  }
});

/* Header bg change after page starts scrolling */

const header = document.querySelector('.header');

let scrollPos = 0;

window.addEventListener('scroll', function(){
  if ((document.body.getBoundingClientRect()).top > scrollPos) {
		header.classList.remove('hidden');
  }
	else {
		header.classList.add('hidden');
  }

	scrollPos = (document.body.getBoundingClientRect()).top;
});

/* Coctail Bar Slider */

let coctailBarSlider = new Swiper('.coctailBarCoctailsSlider', {
  centeredSlides: true,
  loop: true,
  effect: 'slide',
  speed: 400,
  grabCursor: true,
  breakpoints: {
    320: {
      slidesPerView: 1,
    },
    769: {
      slidesPerView: 2,
    },
    1051: {
      slidesPerView: 3,
    },
  },
  on: {
    init: function() {
      const activeSlide = document.querySelector('.coctailBarCoctailsSlider .swiper-slide-active');
      const tequilas = document.querySelectorAll('.coctailBarTequilasItem');

      tequilas.forEach(function(item) {
        if(item.dataset.tequila === activeSlide.dataset.tequila) {
          item.classList.add('active');
        }
      });
    },
    slideChange: function() {
      setTimeout(function() {
        const activeSlide = document.querySelector('.coctailBarCoctailsSlider .swiper-slide-active');
        const tequilas = document.querySelectorAll('.coctailBarTequilasItem');
        const activeTequila = document.querySelector('.coctailBarTequilasItem.active');

        if(activeTequila.dataset.tequila !== activeSlide.dataset.tequila) {
          activeTequila.classList.remove('active');
        }

        tequilas.forEach(function(item) {
          if(item.dataset.tequila === activeSlide.dataset.tequila) {
            item.classList.add('active');
          }
        });
      },0);
    },
  }
});

const tequilaItems = document.querySelectorAll('.coctailBarTequilasItem');

tequilaItems.forEach(function(item) {
  item.addEventListener('click', function(e) {
    if(!e.target.classList.contains('active')) {
      let slides = document.querySelectorAll('.coctailBarItem');

      slides.forEach(function(slide) {
        if(slide.dataset.tequila === item.dataset.tequila) {
          coctailBarSlider.slideTo(parseInt(slide.dataset.swiperSlideIndex) + 1,400, true);
          return;
        }
      });
    }
  });
});


/* FAQ */

const faqItems = document.querySelectorAll('.faqItem');

faqItems.forEach(function(item) {
  item.addEventListener('click', function() {
    this.classList.toggle('active');
  });
});

 let map;

 function initMap() {
   map = new google.maps.Map(document.getElementById("map"), {
     center: { lat: 40.7446298, lng: -73.9898557 },
     zoom: 3,
     styles: mapStyles,
     disableDefaultUI: true,
     draggable: false,
     zoomControl: false,
     scrollwheel: false,
     disableDoubleClickZoom: true,
   });

   markerUnitedStates = new google.maps.Marker({
    map:map,
    // draggable:true,
    // animation: google.maps.Animation.DROP,
    position: new google.maps.LatLng(38.8936704, -77.1550037),
    icon: '../img/map-marker.svg' // null = default icon
  });

  markerCanada = new google.maps.Marker({
    map:map,
    // draggable:true,
    // animation: google.maps.Animation.DROP,
    position: new google.maps.LatLng(45.2498124,-76.0811258),
    icon: '../img/map-marker.svg' // null = default icon
  });

  markerLatvia = new google.maps.Marker({
    map:map,
    // draggable:true,
    // animation: google.maps.Animation.DROP,
    position: new google.maps.LatLng(56.9713958, 23.9887375),
    icon: '../img/map-marker.svg' // null = default icon
  });

  markerLithuania = new google.maps.Marker({
    map:map,
    // draggable:true,
    // animation: google.maps.Animation.DROP,
    position: new google.maps.LatLng(54.7006033, 25.1126084),
    icon: '../img/map-marker.svg' // null = default icon
  });

  markerEstonia = new google.maps.Marker({
    map:map,
    // draggable:true,
    // animation: google.maps.Animation.DROP,
    position: new google.maps.LatLng(59.4713933, 24.4580691),
    icon: '../img/map-marker.svg' // null = default icon
  });
 }

let mapStyles = [
  {
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#050304"
      }
    ]
  },
  {
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#050304"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "off"
        // "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#201619"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#0F080A"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }
];

const mapPoints = {
  unitedStates: {
    lat: 38.8936704, 
    lng: -77.1550037,
  },
  canada: {
    lat: 45.2498124,
    lng: -76.0811258,
  },
  latvia: {
    lat: 56.9713958, 
    lng: 23.9887375,
  },
  lithuania: {
    lat: 54.7006033, 
    lng: 25.1126084,
  },
  estonia: {
    lat: 59.4713933, 
    lng: 24.4580691,
  },
  default: {
    lat: 40.7446298, 
    lng: -73.9898557,
  }
}

const whereToBuyItems = document.querySelectorAll('.whereToBuyItem');
let prevItem;

whereToBuyItems.forEach(function(item) {
  item.addEventListener('mouseover', function() {
    map.setCenter({
      lat: mapPoints[this.dataset.country].lat, 
      lng: mapPoints[this.dataset.country].lng
    });
    map.panBy(-50, 10);
    map.setZoom(6);
  });

  item.addEventListener('mouseout', function() {
    map.setZoom(3);
    map.setCenter({
      lat: mapPoints.default.lat, 
      lng: mapPoints.default.lng
    });
  });

  item.addEventListener('click', function() {
    if(this.classList.contains('active')) {
      map.setCenter({
        lat: mapPoints.default.lat, 
        lng: mapPoints.default.lng
      });
      map.setZoom(3);
      this.classList.remove('active');
    }
    else {
      this.classList.add('active');
    }
  });
});

// map.setCenter(marker.getPosition());

/*
Us -   40.7446298, -73.9898557
canada - 49.2540769,-123.0135506
latvia - 56.9717584, 24.1606074
lithuania - 54.6437511, 25.2670606
estonia - 59.4266401, 24.8166926

*/